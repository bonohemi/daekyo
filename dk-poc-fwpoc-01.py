from __future__ import print_function

import base64
import json
import re

print('Loading function')


def lambda_handler(event, context):
    for record in event['Records']:
        # Kinesis data is base64 encoded so decode here
        print(record['kinesis']['data'])
        # payload = base64.b64decode(record['kinesis']['data'].encode())
        payload = base64.b64decode(record['kinesis']['data'])
        print(type(payload))
        print("Decoded payload: " + payload.decode())
    return 'Successfully processed {} records.'.format(len(event['Records']))

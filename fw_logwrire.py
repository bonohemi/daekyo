#-*- coding: utf-8 -*-
import sys
import argparse
import faker
import time
from datetime import date 
from tqdm import tnrange
from time import sleep


def main():
	print(firewallData())

def firewallData():
	fake =  faker.Faker("ko_KR")
	emptyData = ''
	equipList = [
		'Daekyo_FW_1', 
		'Daekyo_FW_2', 
		'Daekyo_FW_3', 
		'Daekyo_FW_4'
	]
	# firewallPolicy = []
	natPolicy = ['',117]

	st_date = date(2018, 1, 1),
	en_date = date(2018, 12, 31)

	print(type(st_date))

	fakeFirewallLog = f''.join(
		[
			f'{fake.date(pattern="%Y-%m-%d %H:%M", end_datetime=None)},',	# 시작시간
			f'{fake.date(pattern="%Y-%m-%d %H:%M", end_datetime=None)},',	# 종료시간
			f'{fake.random_int(min=0, max=100)},',							# 유지시간
			f'{fake.random_element(elements=equipList)},',
			f'{fake.ipv4_private(network=False, address_class=None)},',
			f'{fake.random_int(min=1024, max=49151)},',
			f'{fake.date_time_between_dates(datetime_start=date(2018, 1, 1), datetime_end=date(2018, 12, 31), tzinfo=None)},',
			f'{fake.random_int(min=1, max=4000)},',							# 방화벽 정책 ID
			f'{fake.random_element(elements=natPolicy)},',					# NAT 정책 ID
			f'{emptyData},',
			f'{emptyData},',
			f'{emptyData},',
			f'{emptyData},',
			f'{emptyData},',
			f'{fake.ipv4_public(network=False, address_class=None)}'
		]
	)

	print(fake.date_time_between_dates(
		datetime_start=date(1998, 1, 1),
		datetime_end=date(2013, 1, 1)
	).date())

	return fakeFirewallLog

def firewallDataTest():
	for i in tnrange(100, desc='test'):
		print(firewallData())

# 'Expedita at beatae voluptatibus nulla omnis.'

# fake.random_element(elements=my_word_list)
# fake.random_int(min=0, max=9999)

if __name__ == "__main__":
	# main()
	firewallDataTest()
	print(time.tzname)
	
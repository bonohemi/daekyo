★ 매우 중요
인덱스 기본 설정은 기본 샤드 5개 및 복제본 1개입니다. 기본값 이외의 설정을 지정하려면 문서를 추가하기 전에 인덱스를 생성합니다.
PUT elasticsearch_domain/more-movies
{"settings": {"number_of_shards": 6, "number_of_replicas": 2}}





이 첫 번째 예에서는 다음 Amazon ES 도메인 구성을 보여줍니다.
Elasticsearch 버전 5.5에서 weblogs라는 Amazon ES 도메인을 만듭니다.
인스턴스 유형이 m4.large.elasticsearch인 인스턴스 2개를 사용하여 도메인을 채웁니다.
각 데이터 노드의 저장에 100GiB 자기 디스크 EBS 볼륨을 사용합니다.
단일 IP 주소(192.0.2.0/32)의 익명 액세스만 허용합니다.
aws es create-elasticsearch-domain --domain-name weblogs --elasticsearch-version 5.5 --elasticsearch-cluster-config  InstanceType=m4.large.elasticsearch,InstanceCount=2 --ebs-options EBSEnabled=true,VolumeType=standard,VolumeSize=100 --access-policies '{"Version": "2012-10-17", "Statement": [{"Action": "es:*", "Principal":"*","Effect": "Allow", "Condition": {"IpAddress":{"aws:SourceIp":["192.0.2.0/32"]}}}]}'

Elasticsearch 버전 5.5에서 weblogs라는 Amazon ES 도메인을 만듭니다.
인스턴스 유형이 m4.large.elasticsearch인 인스턴스 6개를 사용하여 도메인을 채웁니다.
각 데이터 노드의 저장에 100GiB 범용(SSD) EBS 볼륨을 사용합니다.
사용자 AWS 계정 ID가 555555555555인 단일 사용자로 서비스에 대한 액세스를 제한합니다.
가용 영역 세 개에 인스턴스 분산
aws es create-elasticsearch-domain --domain-name weblogs --elasticsearch-version 5.5 --elasticsearch-cluster-config  InstanceType=m4.large.elasticsearch,InstanceCount=6,ZoneAwarenessEnabled=true,ZoneAwarenessConfig={AvailabilityZoneCount=3} --ebs-options EBSEnabled=true,VolumeType=gp2,VolumeSize=100 --access-policies '{"Version": "2012-10-17", "Statement": [ { "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::555555555555:root" }, "Action":"es:*", "Resource": "arn:aws:es:us-east-1:555555555555:domain/logs/*" } ] }'




aws es create-elasticsearch-domain --domain-name dk-poc-fwlog-es-01 --elasticsearch-version 6.4 --elasticsearch-cluster-config  InstanceType=m4.large.elasticsearch,InstanceCount=2 --ebs-options EBSEnabled=true,VolumeType=standard,VolumeSize=100 






curl -XPUT https://search-dk-poc-firewall-es-01-hhc46i7obphkzsx7iayzvgjrdy.ap-northeast-2.es.amazonaws.com/ksk_index
curl -XPUT https://search-dk-poc-firewall-es-01-hhc46i7obphkzsx7iayzvgjrdy.ap-northeast-2.es.amazonaws.com/ksk_index/fwlog


curl -XDELETE https://search-dk-poc-firewall-es-01-hhc46i7obphkzsx7iayzvgjrdy.ap-northeast-2.es.amazonaws.com/ksk_index

curl -XGET https://search-dk-poc-firewall-es-01-hhc46i7obphkzsx7iayzvgjrdy.ap-northeast-2.es.amazonaws.com/ksk_index/_template


curl -X 
PUT my_index
{
  "mappings": {
    "properties": {
      "date": {
        "type": "date" 
      }
    }
  }
}


curl -XPUT "https://search-dk-poc-firewall-es-01-hhc46i7obphkzsx7iayzvgjrdy.ap-northeast-2.es.amazonaws.com/ksk_index/user/1" -d'
{
  "name": "wonwoo",
  "email": "wonwoo@test.com"
}'


curl -XPUT 'https://search-dk-poc-firewall-es-01-hhc46i7obphkzsx7iayzvgjrdy.ap-northeast-2.es.amazonaws.com/ksk_index?pretty' -d'
{
    "settings" : {
        "index" : {
            "number_of_shards" : 3, 
            "number_of_replicas" : 2 
        }
    }
}'


put ksk_index
{
    "settings" : {
        "index" : {
            "number_of_shards" : 3, 
            "number_of_replicas" : 2 
        }
    }
}

post ksk_index/fwlog
{
    "mappings" : {
        "type1" : {
            "properties" : {
                "field1" : { "type" : "text" }
            }
        }
    }
}

인덱스를 생성할 때 1개 이상의 매핑을 정의할 수 있습니다.
curl -XPUT 'localhost:9200/test?pretty' -d'
{
    "settings" : {
        "number_of_shards" : 1
    },
    "mappings" : {
        "type1" : {
            "properties" : {
                "field1" : { "type" : "text" }
            }
        }
    }
}'

인덱스를 생성할 때 1개 이상의 매핑을 정의할 수 있습니다.
PUT 'localhost:9200/test?pretty' -d'
{
    "settings" : {
        "number_of_shards" : 1
    },
    "mappings" : {
        "type1" : {
            "properties" : {
                "field1" : { "type" : "text" }
            }
        }
    }
}'
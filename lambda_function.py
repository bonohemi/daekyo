'''
	@date: 2019.04.05
	@author: sungkuk.kim@bespinglobal.com
'''
from __future__ import print_function

import base64
from re import compile as RegularExpression
import json
# from json import dumps as toJson
from myLib import is_digit

print('Loading function')




def lambda_handler(event, context):
	results = []
	succeeded_record_cnt = 0
	
	# 데이터는 버퍼단위로 전달된다.
	for record in event['records']:
		# Kinesis Agent는 base64 encode 하여 전송함
		print(f"event['records']=>record['data']: {record['data']}")
		
		# base64 decode된 bytes 
		byteData = base64.b64decode(record['data'])
		print(base64.b64decode(record['data']))

		
		# print(type(byteData))
		
		# bytes 타입을 문자열로 decode => 문자열 파싱 후 json 변환을 위해
		strFwLog = byteData.decode()
		# strFwLog = byteData.decode().rstrip('\n') # 줄바꿈 문자 제거
		print(f'strData(fwLog: {strFwLog}')
		# print(strFwLog)
		
		# fwLog 파싱
		intSplitPos = strFwLog.find('start_time')
		strFwLogHead = strFwLog[:intSplitPos]
		strFwLogTail = strFwLog[intSplitPos:]
		
		strAllowDenyString = strFwLogHead.split(' ')[3].replace('[','').replace(']','')
		print(f'strAllowDenyString:[{strAllowDenyString}]')
		
		regExpression = RegularExpression(r"\b(\w+)\s*=\s*([^=]*)(?=\s+\w+\s*=|$)")
		dictFwLog = dict(regExpression.findall(strFwLogTail))
		
		# 각 필드 정제
		for k in dictFwLog:
			dictFwLog[k] = dictFwLog[k].strip()
			dictFwLog[k] = dictFwLog[k].replace('"', '')
			if dictFwLog[k] == '-':
				dictFwLog[k] = ''
			
			if is_digit(dictFwLog[k]):
				dictFwLog[k] = int(dictFwLog[k])
			
			# print(f'{k}:{dictFwLog[k]}')
		# 없을 수 있는 필드 생성
		if   "packets_backward" not in dictFwLog.keys():
			dictFwLog['packets_backward'] = 0
		if   "bytes_backward" not in dictFwLog.keys():
			dictFwLog['bytes_backward'] = 0
		
		# 허용차단 구분 추가
		dictFwLog['allow_deny_flag'] = strAllowDenyString[strAllowDenyString.find('_')+1:]

		# 딕셔너리로 변환된 fw log를 json으로 변환
		jsonFwLog = json.dumps(dictFwLog)
		print(f'jsonFwLog->{jsonFwLog}')

		succeeded_record_cnt += 1

		result = {
			'recordId': record['recordId'],
			'result': 'Ok',
			# 'data': jsonFwLog.encode('base64')
			'data': base64.b64encode(json.dumps(dictFwLog))
		}
		
		results.append(result)

	print(f'변환완료 처리 카운트 {succeeded_record_cnt}')
	# print(f'results->{results}')
	
	return {'records': results }
// index 조회
curl -XGET http://localhost:9200/classes?pretty

// index 생성
curl -XPUT http://localhost:9200/classes

// index 삭제
curl -XDELETE http://localhost:9200/classes

curl -XPOST http://localhost:9200/classes/class/1/ -d '{"title":"data", "subject":"data2"}'

curl -XPOST http://localhost:9200/classes/class/1/ -d @oneclass.json

// classes => index
// class => type
// 1 => id
curl -XPOST http://localhost:9200/classes/class/1/ -d '{"title":"data", "subject":"data2"}'

// update - 필드추가
curl -XPOST http://localhost:9200/classes/class/1/_update?pretty -d '{"doc": {"unit:1}}'

// update - 수정
curl -XPOST http://localhost:9200/classes/class/1/_update?pretty -d '{"doc": {"unit:2}}'

// 좀더 프로그램스러운 수정
curl -XPOST http://localhost:9200/classes/class/1/_update?pretty -d '{"script": "ctx._source.unit += 5"}'


///////////////////////////////////////
// 매핑
///////////////////////////////////////
curl -XPOST "http://localhost:9200/classes/class/_mapping' -d @firewall_log_mapping.json

// 크러스터 상태 조회
http://127.0.0.1:9200/_cluster/health?pretty=true


인덱스 기본 설정은 기본 샤드 5개 및 복제본 1개입니다. 기본값 이외의 설정을 지정하려면 문서를 추가하기 전에 인덱스를 생성합니다.

PUT elasticsearch_domain/more-movies
{"settings": {"number_of_shards": 6, "number_of_replicas": 2}}


Elasticsearch 버전 6.0 이상에서 생성한 인덱스에는 문서 유형만 포함될 수 있습니다. 새 Elasticsearch 버전과의 호환성을 극대화하려면 다음과 같이 모든 인덱스에 대해 단일 유형 _doc를 사용합니다.

PUT elasticsearch_domain/more-movies/_doc/1
{"title": "Back to the Future"}

Elasticsearch에서는 아직 존재하지 않는 인덱스에 문서를 추가할 때 자동 인덱스 생성을 사용합니다. 이와 함께, 요청에 ID를 지정하지 않은 경우에는 자동 ID 생성을 적용합니다. 이 간단한 예제에서는 movies 인덱스를 자동으로 생성하고, movie 문서 유형을 정의하고, 문서를 인덱싱하고, 고유한 ID를 지정합니다.

POST elasticsearch_domain/movies/movie
{"title": "Spirited Away"}



인덱스 설정
인덱스 별로 특정 설정을 지정하여 생성할 수 있습니다.
curl -XPUT 'localhost:9200/twitter?pretty' -d'
{
    "settings" : {
        "index" : {
            "number_of_shards" : 3, 
            "number_of_replicas" : 2 
        }
    }
}'
import smart_open
import logging
import argparse
from time import sleep

parser = argparse.ArgumentParser()
parser.add_argument('-ss', '--sleep_second', dest='sleep_second', default='1', type=float, help = '전송지연/초 (default: %(default)s)')
parser.add_argument('-sn', '--start_no', dest='start_no', default='0', type=int, help = '0~3 사이의 숫자')
parser.add_argument('-fn', '--file_name', dest='file_name', default='Daekyo_FW_1.log', help = 'Daekyo_FW_1.log')
args = parser.parse_args()

ACCESS_KEY='AKIAY3FHK4CRBITVZD3C'
SECRET_KEY='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'

writeFileName = args.file_name
dataSetNo = args.start_no
startNum = dataSetNo * 100
stopNum = startNum + 100

totalDataLineCount = 0

writeFileName = f'/firewall_log/log/Daekyo_FW_{dataSetNo+1}.log'

with open(writeFileName, 'wb+') as writeFile:
	for fileNum in range(startNum, stopNum):
		if fileNum == 0:
			fileNum = ''
		try:
			strBucket = 'dk-poc-s3'
			strBucketPathObjectName = f'{strBucket}/syslog/syslog{fileNum}'
			s3Uri = f's3://{ACCESS_KEY}:{SECRET_KEY}@{strBucketPathObjectName}'
			with smart_open.smart_open(s3Uri) as s3File:
				lineCount = 0
				for line in s3File:
					lineCount += 1
					writeFile.write(line)
					writeFile.flush()
					sleep(args.sleep_second)
				totalDataLineCount += lineCount
				fileLineCount = f's3://{strBucketPathObjectName} Count : {lineCount:10,}'
				print(fileLineCount)
		except Exception as ex:
			print('에러가 발생 했습니다', ex)
			logging.error(f'Error Info : {ex}')

print(f'Total Data Count : {totalDataLineCount:10,}')
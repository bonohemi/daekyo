import smart_open
import logging
from time import sleep
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-ss', '--sleep_second', dest='sleep_second', default='1', type=float, help = '전송지연/초 (default: %(default)s)')
parser.add_argument('-sn', '--start_no', dest='start_no', default='0', type=float, help = '0~3 사이의 숫자')
parser.add_argument('-fn', '--file_name', dest='file_name', default='0', type=float, help = 'Daekyo_FW_1.log')
args = parser.parse_args()

ACCESS_KEY='AKIAY3FHK4CRBITVZD3C'
SECRET_KEY='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'

writeFileName = args.file_name

dataSetNo = int(args.start_no)
startNum = dataSetNo * 100
stopNum = startNum + 100

totalDataLineCount = 0

with open('lineInfo.txt', 'w+') as lineInfo:
	for fileNum in range(startNum, stopNum):

		if fileNum == 0:
			fileNum = ''

		# S3 Object Check
		try:
			strBucket = 'dk-poc-s3'
			strBucketPathObjectName = f'{strBucket}/syslog/syslog{fileNum}'
			s3Uri = f's3://{ACCESS_KEY}:{SECRET_KEY}@{strBucketPathObjectName}'
			# print(f'syslog{fileNum}')
			# with smart_open.smart_open(s3Uri) as s3File, open(writeFileName, 'wb') as writeFile:
			# with smart_open.smart_open(s3Uri) as s3File, open('lineInfo.txt', 'a+', buffering=bufsize) as lineInfo:
			# bufsize = 512 # default 8i92
			with smart_open.smart_open(s3Uri) as s3File:
				# print(f'strBucketPathObjectName ==> s3://{strBucketPathObjectName}')
				# print(f's3Uri:{s3Uri}')
				lineCount = 0

				for line in s3File:
					lineCount += 1

				totalDataLineCount += lineCount
				fileLineCount = f's3://{strBucketPathObjectName} Count : {lineCount:10,}'
				print(fileLineCount)
				lineInfo.writelines(fileLineCount+'\n')
				lineInfo.flush()

				# for line in s3File:
				# 	print(line)
				# 	writeFile.write(line)
		except Exception as ex: 				# 에러 종류
			print('에러가 발생 했습니다', ex) 	# ex는 발생한 에러의 이름을 받아오는 변수
			logging.error(f'Error Info : {ex}')

print(f'Total Data Count : {totalDataLineCount:10,}')

# 엑세스키가 파일로 저장되어 있는 경우 아래의 샘플도 동작함
# import smart_open
# for line in smart_open.smart_open('s3://dk-poc-s3/syslog/syslog'):
#         print(line)



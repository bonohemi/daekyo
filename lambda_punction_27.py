from __future__ import print_function

import base64
import json
from re import compile as RegularExpression
from myLib import is_digit
import collections

print('Loading function')

event_data = {
  "invocationId": "invocationIdExample",
  "deliveryStreamArn": "arn:aws:kinesis:EXAMPLE",
  "region": "ap-northeast-2",
  "records": [
    {
      "recordId": "49546986683135544286507457936321625675700192471156785154",
      "approximateArrivalTimestamp": 1495072949453,
      "data": "MjAzLjIzMy42My4yNTMJTWFyIDI2IDEyOjQ5OjA4CTEJdXNlcglpbmZvCQkyMDE5LTAzLTI2VDAzOjQ5OjAyLjg1MzQwNFogW2Z3NF9hbGxvd10gW11zdGFydF90aW1lPSIyMDE5LTAzLTI2IDEyOjQ3OjI1IiBlbmRfdGltZT0iMjAxOS0wMy0yNiAxMjo0ODo1OCIgZHVyYXRpb249OTMgbWFjaGluZV9uYW1lPURhZWt5b19GV18xIGZ3X3J1bGVfaWQ9MzM5NyBuYXRfcnVsZV9pZD0tIHNyY19pcD0xMTkuMTk2LjI1LjMgdXNlcmlkPSBzcmNfcG9ydD0yNjg2OSBkc3RfaXA9MjEwLjEwNi4yMzAuNjUgZHN0X3BvcnQ9NDQzIHByb3RvY29sPVRDUCBpbmdyZXNfaWY9RVhUIGlucHV0IGludGVyZmFjZT1ib25kMCBwYWNrZXRzX2ZvcndhcmQ9MTcgcGFja2V0c19iYWNrd2FyZD0xNCBieXRlc19mb3J3YXJkPTUwMzUgYnl0ZXNfYmFja3dhcmQ9ODE0NiBmcmFnbWVudF9pbmZvPSAgZmxhZ19yZWNvcmQ9M1dheSBPSyAvIEZJTjEgW1NBOlNBRl0gdGVybWluYXRlX3JlYXNvbj0tIAo="
    }
  ]
}

def lambda_handler(event, context):
	output = []

	for record in event['records']:
		print('recordId:{}'.format(record['recordId']))
		print('data:{}'.format(record['data']))

		payload = base64.b64decode(record['data'])
		print('base64.b64decod:{}'.format(payload))

		# Do custom processing on the payload here
		intSplitPos = payload.find('start_time')
		strFwLogHead = payload[:intSplitPos]
		strFwLogTail = payload[intSplitPos:]
		
		strAllowDenyString = strFwLogHead.split(' ')[3].replace('[','').replace(']','')
		
		regExpression = RegularExpression(r"\b(\w+)\s*=\s*([^=]*)(?=\s+\w+\s*=|$)")
		dictFwLog = collections.OrderedDict(regExpression.findall(strFwLogTail))

		for k in dictFwLog:
			dictFwLog[k] = dictFwLog[k].strip()
			dictFwLog[k] = dictFwLog[k].replace('"', '')
			if dictFwLog[k] == '-':
				dictFwLog[k] = ''
			
			if is_digit(dictFwLog[k]):
				dictFwLog[k] = int(dictFwLog[k])

		if "packets_backward" not in dictFwLog.keys():
			dictFwLog['packets_backward'] = 0

		if "bytes_backward" not in dictFwLog.keys():
			dictFwLog['bytes_backward'] = 0

		# if not dictFwLog.has_key('bytes_backward'):
		# 	dictFwLog['bytes_backward'] = 0

		dictFwLog['allow_deny_flag'] = strAllowDenyString[strAllowDenyString.find('_')+1:]
		
		jsonFwLog = json.dumps(dictFwLog)

		print('json==>{}'.format(jsonFwLog))
		
		output_record =collections.OrderedDict()
		output_record = {
			'recordId': record['recordId'],
			'result': 'Ok',
			'data': base64.b64encode(jsonFwLog)
		}
		output.append(output_record)
		print('result:{}'.format(output_record))

	print('Successfully processed {} records.'.format(len(event['records'])))
	print('result:{}'.format(output))

	return {'records': output}

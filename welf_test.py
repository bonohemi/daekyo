# 203.233.63.253	Mar 26 12:52:58	1	user	info		2019-03-26T03:52:52.705676Z [fw4_allow] []start_time="2019-03-26 12:52:37" end_time="2019-03-26 12:52:47" duration=10 machine_name=Daekyo_FW_1 fw_rule_id=76 nat_rule_id=- src_ip=112.146.233.242 userid= src_port=60316 dst_ip=211.234.92.106 dst_port=80 protocol=TCP ingres_if=EXT input interface=bond0 packets_forward=1 packets_backward=6 bytes_forward=70 bytes_backward=416 fragment_info=  flag_record=SYN ACK / Timeout [S:SA] terminate_reason=- 
# 203.233.63.253	Mar 26 12:52:58	1	user	info		2019-03-26T03:52:52.705708Z [fw4_allow] []start_time="2019-03-26 12:52:41" end_time="2019-03-26 12:52:51" duration=10 machine_name=Daekyo_FW_1 fw_rule_id=1646 nat_rule_id=- src_ip=103.255.237.239 userid= src_port=80 dst_ip=210.106.230.43 dst_port=80 protocol=TCP ingres_if=EXT input interface=bond0 packets_forward=1 packets_backward=4 bytes_forward=64 bytes_backward=256 fragment_info=  flag_record=SYN ACK / Timeout [S:SA] terminate_reason=- 

from re import compile as RegularExpression
from json import dumps as toJson
import smart_open

rawData = '203.233.63.253	Mar 26 12:52:58	1	user	info		2019-03-26T03:52:52.705676Z [fw4_allow] []start_time="2019-03-26 12:52:37" end_time="2019-03-26 12:52:47" duration=10 machine_name=Daekyo_FW_1 fw_rule_id=76 nat_rule_id=- src_ip=112.146.233.242 userid= src_port=60316 dst_ip=211.234.92.106 dst_port=80 protocol=TCP ingres_if=EXT input interface=bond0 packets_forward=1 packets_backward=6 bytes_forward=70 bytes_backward=416 fragment_info=  flag_record=SYN ACK / Timeout [S:SA] terminate_reason=- '

strData = rawData.split('\t')[6]

strHead = rawData[:rawData.find('start_time')]
strTail = rawData[rawData.find('start_time'):]

print(f'HEAD:{strHead}')
print(f'TAIL:{strTail}')

# for xxx in strHead.split(' '):
# 	print(f'sss:{xxx}')

strAllowDenyString = strHead.split(' ')[3].replace('[','').replace(']','')

print(f'strAllowDenyString:{strAllowDenyString}')

# for data in listRawData:
# 	print(data)

# print('+++++++++++++++++++++++++++++++++++++++')
# print(listRawData[6])

# data = 'start_time="2019-03-26 12:52:37" end_time="2019-03-26 12:52:47" duration=10 machine_name=Daekyo_FW_1 fw_rule_id=76 nat_rule_id=- src_ip=112.146.233.242 userid= src_port=60316 dst_ip=211.234.92.106 dst_port=80 protocol=TCP ingres_if=EXT input interface=bond0 packets_forward=1 packets_backward=6 bytes_forward=70 bytes_backward=416 fragment_info=  flag_record=SYN ACK / Timeout [S:SA] terminate_reason=- '
# data = 'start_time="2019-03-26 12:52:37" end_time="2019-03-26 12:52:47" duration=10 machine_name=Daekyo_FW_1 '
# data = 'start_time="2019-03-26 12:52:37" end_time="2019-03-26 12:52:47" duration=10 machine_name=Daekyo_FW_1 fragment_info=1  flag_record=SYN ACK / Timeout [S:SA] terminate_reason=- '

# 첫번째 시도
# print(data.split("="))

# 두번째 시도
# r = 'start_time="2019-03-26 12:52:37" end_time="2019-03-26 12:52:47" duration=10 machine_name=Daekyo_FW_1 fw_rule_id=76 nat_rule_id=- src_ip=112.146.233.242 userid= src_port=60316 dst_ip=211.234.92.106 dst_port=80 protocol=TCP ingres_if=EXT input interface=bond0 packets_forward=1 packets_backward=6 bytes_forward=70 bytes_backward=416 fragment_info=  flag_record=SYN ACK / Timeout [S:SA] terminate_reason=- '
# lis=r.split('=')
# dic={}
# try :
#  for i,x in enumerate(reversed(lis)):
#     i+=1
#     slast=lis[-(i+1)]
#     slast=slast.split()
#     dic[slast[-1]]=x
#     lis[-(i+1)]=" ".join(slast[:-1])
# except IndexError:pass    
# print(dic)

# 세번째 시도 - 역시 난 프로그래머야 ㅋㅋㅋㅋ
regExpression = RegularExpression(r"\b(\w+)\s*=\s*([^=]*)(?=\s+\w+\s*=|$)")
# print(dict(regex.findall(data)))
# data = data.replace('"','')
dictFirewallLog = dict(regExpression.findall(strData.replace('"','')))

# for k,v in dictFirewallLog.items():
# 	print(f'key->{k} : [{v}]')
print('======================================================================')

jsonFirewallLog = toJson(dictFirewallLog)
print(f'json->[{jsonFirewallLog}]')

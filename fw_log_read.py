'''
    @date: 2019.04.05
    @author: sungkuk.kim@bespinglobal.com
'''
from __future__ import print_function

import base64
from re import compile as RegularExpression
from json import dumps as toJson

print('Loading function')

def lambda_handler(event, context):
    
    # for k, v in event.items():
    #     print(f'key=>{k}')
    
    for record in event['records']:
        # Kinesis Agent는 base64 encode 하여 전송함
        print(f"event['records']=>record['data']: {record['data']}")
        
        # base64 decode된 bytes 
        byteData = base64.b64decode(record['data'])
        # print(type(byteData))
        
        # bytes 타입을 문자열로 decode : 문자열 파싱 후 json 변환을 위해
        strFwLog = byteData.decode()
        print(f'strData(fwLog: {strFwLog}')
        
        # fwLog 파싱
        intSplitPos = strFwLog.find('start_time')
        strFwLogHead = strFwLog[:intSplitPos]
        strFwLogTail = strFwLog[intSplitPos:]
        
        strAllowDenyString = strFwLogHead.split(' ')[3].replace('[','').replace(']','')
        print(f'strAllowDenyString:[{strAllowDenyString}]')
        
        regExpression = RegularExpression(r"\b(\w+)\s*=\s*([^=]*)(?=\s+\w+\s*=|$)")
        dictFwLog = dict(regExpression.findall(strFwLogTail.replace('"','')))

        # 딕셔너리로 변환된 fw log를 json으로 변환
        jsonFwLog = toJson(dictFwLog)
        print(f'jsonFwLog->{jsonFwLog}')

    return jsonFwLog
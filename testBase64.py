# -*- coding: utf-8 -*-
import base64
import sys
# reload(sys)
# sys.setdefaultencoding('utf-8')

# Encode string data
def stringToBase64(s):
	return base64.b64encode(s.encode('utf-8'))

def base64ToString(b):
	return base64.b64decode(b).decode('utf-8')


# Encode binary file data
def fileToBase64(filepath):
	fp = open(filepath, "rb")
	data = fp.read()
	fp.close()
	return base64.b64encode(data).decode('utf-8')

strData = '김성국'

print(strData.decode('utf-8').encode('utf-8'))

# prin type(strData.encode('utf-8'))


# base64Encoding = base64.b64encode(strData.encode('utf-8'))

# base64Encoding = base64.b64encode(strData)
# print(type(base64Encoding))
# print(base64Encoding)

# base64Decoding = base64.b64decode(base64Encoding)
# print(type(base64Decoding))
# print(base64Decoding)

# print(type(strData).__name__)
# print(type(u'한글').__name__)

# uniData = unicode(strData,'utf-8')

# base64Encoding = base64.b64encode(uniData.encode('utf-8'))
# print('base64Encoding:{}'.format(base64Encoding))

# base64Decoding = base64.b64decode(base64Encoding)

# print('base64Encoding:{}'.format(base64Decoding.decode('utf-8')))

# strData2 = strData.encode('utf-8')
# # utfData = strData.encode('utf-8')

# base64Encoding = base64.b64encode(strData)
# base64Encoding = stringToBase64(strData)
# print('base64Encoding:{}'.format(base64Encoding))


# base64Decoding = base64.b64encode(base64Encoding)

# print(type(base64Decoding))
# print(base64Decoding)

# print('base64Decoding:{}'.format(base64Decoding))


